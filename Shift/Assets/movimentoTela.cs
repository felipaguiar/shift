﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movimentoTela : MonoBehaviour {
    private bool direcao = false;
    public float velocidade = 1;
	
	// Update is called once per frame
	void Update () {
        if (direcao == false)
            transform.Translate(-velocidade , 0 , 0);
        else
            transform.Translate(velocidade, 0, 0);
        if (transform.position.x < -10)
            direcao = true;
        else if (transform.position.x > 165)
            direcao = false;
    }

}

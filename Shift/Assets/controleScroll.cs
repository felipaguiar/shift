﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class controleScroll : MonoBehaviour {
    public GameObject scrollA;
    public GameObject scrollB;
    public GameObject scrollC;

    public void trocarScrollA()
    {
        scrollA.SetActive(true);
        scrollB.SetActive(false);
        scrollC.SetActive(false);
    }
    public void trocarScrollB()
    {
        scrollA.SetActive(false);
        scrollB.SetActive(true);
        scrollC.SetActive(false);
    }
    public void trocarScrollC()
    {
        scrollA.SetActive(false);
        scrollB.SetActive(false);
        scrollC.SetActive(true);
    }
}

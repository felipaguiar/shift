﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class controleTela : MonoBehaviour {
    public GameObject painelA;
    public GameObject painelB;
    public GameObject painelC;
    public GameObject painelD;
    public GameObject painelP1;
    public GameObject painelP2;
    public GameObject painelP3;
    public GameObject painelP4;
    public GameObject painelP5;
    public GameObject painelP6;
    public GameObject painelP7;
    public GameObject painelP8;
    public GameObject painelP9;
    public GameObject painelP10;
    public GameObject popupCerto;
    public GameObject popupErrado;
    public GameObject popupCerto2;
    public GameObject popupErrado2;
    public GameObject popupCerto3;
    public GameObject popupErrado3;
    public GameObject popupCerto4;
    public GameObject popupErrado4;
    public GameObject popupCerto5;
    public GameObject popupErrado5;
    public GameObject popupCerto6;
    public GameObject popupErrado6;
    public GameObject popupCerto7;
    public GameObject popupErrado7;
    public GameObject popupCerto8;
    public GameObject popupErrado8;
    public GameObject popupCerto9;
    public GameObject popupErrado9;
    public GameObject popupCerto10;
    public GameObject popupErrado10;
    public Button btDay1;
    public Button btDay2;
    public Button btDay3;
    public Button btDay4;
    public Button btDay5;
    public Button btDay6;
    public Button btDay7;
    public Button btDay8;
    public Button btDay9;
    public Button btDay10;
    public GameObject popLoterica;

    //troca de telas
    public void trocarPainelA()
    {
        painelA.SetActive(false);
        painelB.SetActive(true);
    }
    public void trocarPainelB()
    {
        painelB.SetActive(false);
        painelC.SetActive(true);
    }
    public void trocarPainelC()
    {
        painelD.SetActive(true);
        painelC.SetActive(false);
    }
    public void trocarPainelD()
    {
        painelD.SetActive(false);
        painelC.SetActive(true);
    }

    //troca de telas para pergunta 1
    public void trocarPainelP1()
    {
        painelP1.SetActive(true);
        painelC.SetActive(false);

    }
    public void popUpCerto()
    {
        popupCerto.SetActive(true);

    }
    public void popUpErrado()
    {
        popupErrado.SetActive(true);
    }
    public void trocarPainelRespostaCerta()
    {
        popupCerto.SetActive(false);
        painelP1.SetActive(false);
        painelC.SetActive(true);
        btDay1.interactable = !btDay1.enabled;
        btDay2.interactable = btDay2.enabled;

    }
    public void trocarPainelRespostaErrada()
    {
        popupErrado.SetActive(false);
        painelP1.SetActive(false);
        painelC.SetActive(true);
        btDay1.interactable = !btDay1.enabled;
        btDay2.interactable = btDay2.enabled;
    }

    //troca de telas para pergunta 2
    public void trocarPainelP2()
    {
        painelP2.SetActive(true);
        painelC.SetActive(false);

    }
    public void popUpCerto2()
    {
        popupCerto2.SetActive(true);

    }
    public void popUpErrado2()
    {
        popupErrado2.SetActive(true);
    }
    public void trocarPainelRespostaCerta2()
    {
        popupCerto2.SetActive(false);
        painelP2.SetActive(false);
        painelC.SetActive(true);
        btDay2.interactable = !btDay1.enabled;
        btDay3.interactable = btDay2.enabled;
    }
    public void trocarPainelRespostaErrada2()
    {
        popupErrado2.SetActive(false);
        painelP2.SetActive(false);
        painelC.SetActive(true);
        btDay2.interactable = !btDay1.enabled;
        btDay3.interactable = btDay2.enabled;
    }

    //troca de telas para pergunta 3
    public void trocarPainelP3()
    {
        painelP3.SetActive(true);
        painelC.SetActive(false);

    }
    public void popUpCerto3()
    {
        popupCerto3.SetActive(true);

    }
    public void popUpErrado3()
    {
        popupErrado3.SetActive(true);
    }
    public void trocarPainelRespostaCerta3()
    {
        popupCerto3.SetActive(false);
        painelP3.SetActive(false);
        painelC.SetActive(true);
        btDay3.interactable = !btDay1.enabled;
        btDay4.interactable = btDay2.enabled;
    }
    public void trocarPainelRespostaErrada3()
    {
        popupErrado3.SetActive(false);
        painelP3.SetActive(false);
        painelC.SetActive(true);
        btDay3.interactable = !btDay3.enabled;
        btDay4.interactable = btDay4.enabled;
    }

    //troca de telas para pergunta 4
    public void trocarPainelP4()
    {
        painelP4.SetActive(true);
        painelC.SetActive(false);

    }
    public void popUpCerto4()
    {
        popupCerto4.SetActive(true);

    }
    public void popUpErrado4()
    {
        popupErrado4.SetActive(true);
    }
    public void trocarPainelRespostaCerta4()
    {
        popupCerto4.SetActive(false);
        painelP4.SetActive(false);
        painelC.SetActive(true);
        btDay4.interactable = !btDay4.enabled;
        btDay5.interactable = btDay5.enabled;
    }
    public void trocarPainelRespostaErrada4()
    {
        popupErrado4.SetActive(false);
        painelP4.SetActive(false);
        painelC.SetActive(true);
        btDay4.interactable = !btDay4.enabled;
        btDay5.interactable = btDay5.enabled;
    }

    //troca de tela para pergunta 5
    public void trocarPainelP5()
    {
        painelP5.SetActive(true);
        painelC.SetActive(false);

    }
    public void popUpCerto5()
    {
        popupCerto5.SetActive(true);

    }
    public void popUpErrado5()
    {
        popupErrado5.SetActive(true);
    }
    public void trocarPainelRespostaCerta5()
    {
        popupCerto5.SetActive(false);
        painelP5.SetActive(false);
        painelC.SetActive(true);
        btDay5.interactable = !btDay5.enabled;
        btDay6.interactable = btDay6.enabled;
    }
    public void trocarPainelRespostaErrada5()
    {
        popupErrado5.SetActive(false);
        painelP5.SetActive(false);
        painelC.SetActive(true);
        btDay5.interactable = !btDay5.enabled;
        btDay6.interactable = btDay6.enabled;
    }

    //troca de tela para pergunta 6
    public void trocarPainelP6()
    {
        painelP6.SetActive(true);
        painelC.SetActive(false);

    }
    public void popUpCerto6()
    {
        popupCerto6.SetActive(true);

    }
    public void popUpErrado6()
    {
        popupErrado6.SetActive(true);
    }
    public void trocarPainelRespostaCerta6()
    {
        popupCerto6.SetActive(false);
        painelP6.SetActive(false);
        painelC.SetActive(true);
        btDay6.interactable = !btDay6.enabled;
        btDay7.interactable = btDay7.enabled;

    }
    public void trocarPainelRespostaErrada6()
    {
        popupErrado6.SetActive(false);
        painelP6.SetActive(false);
        painelC.SetActive(true);
        btDay6.interactable = !btDay6.enabled;
        btDay7.interactable = btDay7.enabled;
    }

    //troca de tela para pergunta 7
    public void trocarPainelP7()
    {
        painelP7.SetActive(true);
        painelC.SetActive(false);

    }
    public void popUpCerto7()
    {
        popupCerto7.SetActive(true);

    }
    public void popUpErrado7()
    {
        popupErrado7.SetActive(true);
    }
    public void trocarPainelRespostaCerta7()
    {
        popupCerto7.SetActive(false);
        painelP7.SetActive(false);
        painelC.SetActive(true);
        btDay7.interactable = !btDay7.enabled;
        btDay8.interactable = btDay8.enabled;

    }
    public void trocarPainelRespostaErrada7()
    {
        popupErrado7.SetActive(false);
        painelP7.SetActive(false);
        painelC.SetActive(true);
        btDay7.interactable = !btDay7.enabled;
        btDay8.interactable = btDay8.enabled;
    }

    //troca de tela para pergunta 8
    public void trocarPainelP8()
    {
        painelP8.SetActive(true);
        painelC.SetActive(false);

    }
    public void popUpCerto8()
    {
        popupCerto8.SetActive(true);

    }
    public void popUpErrado8()
    {
        popupErrado8.SetActive(true);
    }
    public void trocarPainelRespostaCerta8()
    {
        popupCerto8.SetActive(false);
        painelP8.SetActive(false);
        painelC.SetActive(true);
        btDay8.interactable = !btDay8.enabled;
        btDay9.interactable = btDay9.enabled;

    }
    public void trocarPainelRespostaErrada8()
    {
        popupErrado8.SetActive(false);
        painelP8.SetActive(false);
        painelC.SetActive(true);
        btDay8.interactable = !btDay8.enabled;
        btDay9.interactable = btDay9.enabled;
    }

    //troca de tela para pergunta 9
    public void trocarPainelP9()
    {
        painelP9.SetActive(true);
        painelC.SetActive(false);

    }
    public void popUpCerto9()
    {
        popupCerto9.SetActive(true);

    }
    public void popUpErrado9()
    {
        popupErrado9.SetActive(true);
    }
    public void trocarPainelRespostaCerta9()
    {
        popupCerto9.SetActive(false);
        painelP9.SetActive(false);
        painelC.SetActive(true);
        btDay9.interactable = !btDay9.enabled;
        btDay10.interactable = btDay10.enabled;

    }
    public void trocarPainelRespostaErrada9()
    {
        popupErrado9.SetActive(false);
        painelP9.SetActive(false);
        painelC.SetActive(true);
        btDay9.interactable = !btDay9.enabled;
        btDay10.interactable = btDay10.enabled;
    }

    //troca de tela para pergunta 10
    public void trocarPainelP10()
    {
        painelP10.SetActive(true);
        painelC.SetActive(false);

    }
    public void popUpCerto10()
    {
        popupCerto10.SetActive(true);

    }
    public void popUpErrado10()
    {
        popupErrado10.SetActive(true);
    }
    public void trocarPainelRespostaCerta10()
    {
        popupCerto10.SetActive(false);
        painelP10.SetActive(false);
        painelC.SetActive(true);
        popLoterica.SetActive(true);
        btDay10.interactable = !btDay10.enabled;
        


    }
    public void trocarPainelRespostaErrada10()
    {
        popupErrado10.SetActive(false);
        painelP10.SetActive(false);
        painelC.SetActive(true);
        popLoterica.SetActive(true);
        btDay10.interactable = !btDay10.enabled;
    }



    //tela para lotérica após a ultima pergunta
    public void fecharLoterica()
    {
        popLoterica.SetActive(false);
    }




}

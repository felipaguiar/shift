﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class banco : MonoBehaviour {
    private int dinheiroCC;
    public Text texto;
    public Text texto2;
    public GameObject botao;
    public GameObject popup;
    // Use this for initialization
    public void Start()
    {
        receberDinheiro(0);
    }
  
    public void Update()
    {
        texto.text ="R$: " + dinheiroCC.ToString();
        texto2.text = "R$: " + dinheiroCC.ToString();
    }
    public void pagarConta(int valor)
    {   
        if(dinheiroCC >= valor)
        {
            dinheiroCC -= valor;
        }
    }
    public void receberDinheiro(int valor)
    {
        dinheiroCC += valor;
    }


    
}

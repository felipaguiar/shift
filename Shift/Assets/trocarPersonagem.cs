﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class trocarPersonagem : MonoBehaviour {
    public Sprite[] personagens;
    public Image personagem;
    public Image personagemPrincipal;
    public int indice = 0;
    
    public Sprite[] modelo1;
    public Sprite[] modelo2;
    public Sprite[] modelo3;
    public Sprite[] modelo4;

    public Sprite[] modelo5;
    public Sprite[] modelo6;
    public Sprite[] modelo7;
    public Sprite[] modelo8;

    public Sprite[] modelo9;
    public Sprite[] modelo10;
    public Sprite[] modelo11;
    public Sprite[] modelo12;


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void trocarParaDireita()
    {
        if (indice < personagens.Length -1)
            indice++;
        else
            indice = 0;
        personagem.sprite = personagens[indice];
        personagemPrincipal.sprite = personagens[indice];
    }
    public void trocarParaEsquerda()
    {
        if (indice > 0)
            indice--;
        else
            indice = personagens.Length -1;
        personagem.sprite = personagens[indice];
        personagemPrincipal.sprite = personagens[indice];
    }
    public void trocarRoupa(int tipo)
    {
        switch (indice)
        {
            case 0:
                personagemPrincipal.sprite = modelo1[tipo];
                break;
            case 1:
                personagemPrincipal.sprite = modelo2[tipo];
                break;
            case 2:
                personagemPrincipal.sprite = modelo3[tipo];
                break;
            case 3:
                personagemPrincipal.sprite = modelo4[tipo];
                break;
        }
    }
    public void trocarRoupa2(int tipo)
    {
        switch (indice)
        {
            case 0:
                personagemPrincipal.sprite = modelo5[tipo];
                break;
            case 1:
                personagemPrincipal.sprite = modelo6[tipo];
                break;
            case 2:
                personagemPrincipal.sprite = modelo7[tipo];
                break;
            case 3:
                personagemPrincipal.sprite = modelo8[tipo];
                break;
        }
    }
    public void trocarRoupa3(int tipo)
    {
        switch (indice)
        {
            case 0:
                personagemPrincipal.sprite = modelo9[tipo];
                break;
            case 1:
                personagemPrincipal.sprite = modelo10[tipo];
                break;
            case 2:
                personagemPrincipal.sprite = modelo11[tipo];
                break;
            case 3:
                personagemPrincipal.sprite = modelo12[tipo];
                break;
        }
    }
}
